import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import com.exception.CustomRandomizerException;
import com.exception.NumberRandomizer;

public class LogException {

    private static final Logger log =
            Logger.getLogger(NumberRandomizer.class.getName());

    private static final Logger log2 =
            Logger.getLogger(NumberRandomizer.class.getName());



    public static void main(String[] args) throws CustomRandomizerException, IOException {

        boolean append = true;

        FileHandler handler = new FileHandler("src/logging.log", append);

        log2.addHandler(handler);

        NumberRandomizer numbRandom = new NumberRandomizer();

        try {

            int x = numbRandom.getRandNumber();
            log.info("Приложение успешно запущено");
            log2.info("Приложение успешно запущено");

        } catch (CustomRandomizerException e) {
            log.info(e.getMessage() + e.getRandNumber());
            log2.info(e.getMessage() + e.getRandNumber());
        }


    }
}
