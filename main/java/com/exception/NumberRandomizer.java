package com.exception;

import java.util.Random;

public class NumberRandomizer {

    int number;

    public int getRandNumber() throws CustomRandomizerException {

        number = (int) (Math.random()*10);

        if (number <= 5) {

            throw new CustomRandomizerException("Сгенерированное число – ", number);
        }

            return number;

    }
}
