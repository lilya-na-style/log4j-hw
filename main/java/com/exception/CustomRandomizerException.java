package com.exception;

public class CustomRandomizerException extends Exception{

    private int randNum;

    public CustomRandomizerException(String message, int randNum) {
        super(message);
        this.randNum = randNum;
    }

    public int getRandNumber() throws CustomRandomizerException{

        return randNum;
    }
}
